package br.ucsal.bes20182.testequalidade.restaurante.business;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Test;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20182.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20182.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20182.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.MesaDao;

public class RestauranteBOUnitarioTest {

	// M�todo a ser testado: public Integer abrirComanda(Integer numeroMesa)
	// throws RegistroNaoEncontrado, MesaOcupadaException {
	// Verificar se a abertura de uma comanda para uma mesa livre apresenta
	// sucesso.

	ComandaDao comandaDao = new ComandaDao();
	ItemDao itemDao = new ItemDao();
	MesaDao mesaDao = new MesaDao();
	RestauranteBO restauranteBO;

	@Before
	public void init() throws MesaOcupadaException {
		Mesa mesa;
		Comanda comanda;

		// TODO Inclusão da mesa
		mesa = new Mesa(1);
		mesaDao.incluir(mesa);
		// TODO Inclusão da Comanda
		comanda = new Comanda(mesa);
		comanda.codigo = 1;
		comandaDao.incluir(comanda);

		restauranteBO = new RestauranteBO(comandaDao, itemDao, mesaDao);
	}

	@Test
	public void abrirComandaMesaLivre() {
		Integer numeroMesa = 1;
		try {
			restauranteBO.abrirComanda(numeroMesa);
			assertTrue(true);
		} catch (RegistroNaoEncontrado | MesaOcupadaException e) {
			fail(e.getMessage());
		}

	}
}
