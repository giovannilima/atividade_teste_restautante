package br.ucsal.bes20182.testequalidade.restaurante.business;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Test;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20182.testequalidade.restaurante.domain.Item;
import br.ucsal.bes20182.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20182.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal.bes20182.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20182.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.MesaDao;

public class RestauranteBOIntegradoTest {

	// M�todo a ser testado: public void incluirItemComanda(Integer
	// codigoComanda, Integer codigoItem, Integer qtdItem) throws
	// RegistroNaoEncontrado, ComandaFechadaException {
	// Verificar se a inclus�o de uma item em uma comanda fechada levanta
	// exce��o.

	ComandaDao comandaDao = new ComandaDao();
	ItemDao itemDao = new ItemDao();
	MesaDao mesaDao = new MesaDao();
	RestauranteBO restauranteBO;

	@Before
	public void init() throws MesaOcupadaException {
		Mesa mesa;
		Item item;
		Comanda comanda;

		// TODO Inclusão da mesa
		mesa = new Mesa(1);
		mesaDao.incluir(mesa);
		// TODO Inclusão da Comanda
		comanda = new Comanda(mesa);
		comanda.codigo = 1;
		comandaDao.incluir(comanda);
		// TODO Inclusão do Item
		item = new Item("Pão", 5d);
		item.codigo = 1;
		itemDao.incluir(item);

		restauranteBO = new RestauranteBO(comandaDao, itemDao, mesaDao);
	}

	@Test
	public void incluirItemComandaFechada() {
		Integer numeroMesa = 1;
		Integer codigoItem = 1;
		Integer qtdItem = 1;
		try {

			restauranteBO.abrirComanda(numeroMesa);
			restauranteBO.fecharComanda(numeroMesa);
		} catch (Exception e) {
			// TODO Exceptions dos métodos anteriores
		}

		// TODO Teste da inclusão de um item em uma comanda fechada
		try {
			restauranteBO.incluirItemComanda(numeroMesa, codigoItem, qtdItem);
			assertTrue(true);
		} catch (RegistroNaoEncontrado | ComandaFechadaException e) {
			fail(e.getMessage());
		}
	}

}
