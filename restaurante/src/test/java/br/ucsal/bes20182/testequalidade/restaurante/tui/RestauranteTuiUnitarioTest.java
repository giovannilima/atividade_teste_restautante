package br.ucsal.bes20182.testequalidade.restaurante.tui;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

import br.ucsal.bes20182.testequalidade.restaurante.business.RestauranteBO;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.MesaDao;

public class RestauranteTuiUnitarioTest {

	// M�todo a ser testado: public Integer obterInteiro(String mensagem)
	// Verificar o processo de entrada e sa�da para obten��o de um n�mero
	// inteiro. Obs: lembrar de fazer o mock do System.in e do System.out.
	RestauranteTui restauranteTui;
	RestauranteBO restauranteBO;
	ComandaDao comandaDao = new ComandaDao();
	ItemDao itemDao = new ItemDao();
	MesaDao mesaDao = new MesaDao();
	ByteArrayOutputStream out;

	@Before
	public void init() {
		ByteArrayInputStream in = new ByteArrayInputStream("5\n".getBytes());
		System.setIn(in);

		out = new ByteArrayOutputStream();

		System.setOut(new PrintStream(out));

		restauranteBO = new RestauranteBO(comandaDao, itemDao, mesaDao);
		restauranteTui = new RestauranteTui(restauranteBO);
	}

	@Test
	public void verificarObterInteiro() throws Exception {

		Integer valor = restauranteTui.obterInteiro("Digite um numero:");

		String resultadoAtual = out.toString();
		String resultadoEsperado = "Digite um numero:\n";
		assertEquals(resultadoEsperado, resultadoAtual);
		assertEquals(new Integer(5), valor);
	}
}