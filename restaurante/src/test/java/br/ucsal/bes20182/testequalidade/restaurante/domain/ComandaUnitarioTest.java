package br.ucsal.bes20182.testequalidade.restaurante.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.ucsal.bes20182.testequalidade.restaurante.business.RestauranteBO;
import br.ucsal.bes20182.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.MesaDao;

public class ComandaUnitarioTest {
	// M�todo a ser testado: public Double calcularTotal() {
	// Verificar o c�lculo do valor total, com um total de 4 itens.
	// Obs: lembre-se de substituir o conte�do do atributo itens por uma lista
	// com 4 itens.

	ComandaDao comandaDao = new ComandaDao();
	ItemDao itemDao = new ItemDao();
	MesaDao mesaDao = new MesaDao();
	RestauranteBO restauranteBO;
	Comanda comanda;

	@Before
	public void init() throws MesaOcupadaException {
		itemDao.itens = incluirItens();
		Mesa mesa = new Mesa(1);
		comanda = new Comanda(mesa);
		itensHastMap();
		restauranteBO = new RestauranteBO(comandaDao, itemDao, mesaDao);
	}

	@Test
	public void calcularTotal4Itens() {
		double result = comanda.calcularTotal();
		assertEquals(147.0, result);
	}

	private List<Item> incluirItens() {
		List<Item> itens = new ArrayList<>();
		itens.add(new Item("Pão", 5d));
		itens.add(new Item("Camarão", 70d));
		itens.add(new Item("Batata Frita", 25d));
		itens.add(new Item("Arroz", 17d));
		return itens;
	}

	private void itensHastMap() {
		Item item = itemDao.itens.get(0);
		comanda.itens.put(item, 2);//Total = 10

		item = itemDao.itens.get(1);
		comanda.itens.put(item, 1);//Total = 70

		item = itemDao.itens.get(2);
		comanda.itens.put(item, 2);//Total = 50

		item = itemDao.itens.get(3);
		comanda.itens.put(item, 1);//Total = 17
		//TODO //Total Máximo = 147
	}

}
